package com.wellsfromwales.javatuts.lambda;

import java.util.Date;

public class Person {

    public enum Sex {
        MALE, FEMALE
    }

    String name;
    Date birthday;
    Sex gender;
    String emailAddress;

    public long getAge() {
    	Date now = new Date(System.currentTimeMillis());
    	return now.compareTo(birthday);
    }

    public void printPerson() {
    	System.out.println(toString());
    }

	@Override
	public String toString() {
		return "Person [name=" + name + ", birthday=" + birthday + ", gender="
				+ gender + ", emailAddress=" + emailAddress + "]";
	}

}