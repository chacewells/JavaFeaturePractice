package com.wellsfromwales.javatuts.lambda;

import org.apache.commons.lang3.StringUtils;

public class Calculator {
	  
    interface IntegerMath {
        int operation(int a, int b);   
    }
    
    interface MathConcat {
    	String concatThese(Object a, Object b);
    }
  
    public int operateBinary(int a, int b, IntegerMath op) {
        return op.operation(a, b);
    }
 
    public static void main(String... args) {
    
        Calculator myApp = new Calculator();
        IntegerMath addition = (a, b) -> a + b;
        IntegerMath subtraction = (a, b) -> a - b;
        System.out.println("40 + 2 = " +
            myApp.operateBinary(40, 2, addition));
        System.out.println("20 - 10 = " +
        	myApp.operateBinary(20, 10, subtraction));
        
        MathConcat spaceSeparated = (a, b) -> a + " " + b;
        MathConcat commaSeparated = (a, b) -> {
        	String padded = StringUtils.leftPad(a.toString(), 20);
        	return padded + ", " + b;
        	};
        
        System.out.println(spaceSeparated.concatThese('\u0040', "Blubber"));
        System.out.println(commaSeparated.concatThese("Wells", "Aaron"));
    }
}