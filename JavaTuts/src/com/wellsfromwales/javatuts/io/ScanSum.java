package com.wellsfromwales.javatuts.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Locale;
import java.util.Scanner;

public class ScanSum {

	public static void main(String[] args) throws FileNotFoundException {
		
		Scanner scan = null;
		double sum = 0;
		
		try {
			scan = new Scanner(new FileReader("C:\\Users\\chq-aaronwe\\Documents\\tmp\\usnumbers.txt"));
			scan.useLocale(Locale.US);
			
			while (scan.hasNext()) {
				if (scan.hasNextDouble())
					sum += scan.nextDouble();
				else
					scan.next();
			}
			
		} finally {
			if (scan != null)
				scan.close();
		}
		
		System.out.println(sum);
		
	}

}
